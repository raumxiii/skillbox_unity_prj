using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistonController : MonoBehaviour
{
    private Rigidbody myRigidbody;
    private float timer = 3;
    private float curTime;
    private Vector3 force = new Vector3(0, 7000, 0);
    
    // Start is called before the first frame update
    void Start()
    {
        myRigidbody = transform.GetComponent<Rigidbody>();
        curTime = timer;
    }

    void FixedUpdate()
    {
        curTime -= Time.deltaTime;
        if (curTime <= 0)
        {
            curTime = timer;
            myRigidbody.AddForce(force, ForceMode.Force);
        }        
    }
}
