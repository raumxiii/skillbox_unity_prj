using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExposionSphereController : MonoBehaviour
{

    [Range (10,800)] public float explosionForce;
    [Range(10, 80)] public float explosionRadius;
    [Range(1, 3)] public int explosionCooldown;

    private float timeToExplode;
    private Vector3 explosionPosition;

    void Start()
    {
        timeToExplode = explosionCooldown;
        explosionPosition = transform.position;
    }

    private void FixedUpdate()
    {
        timeToExplode-=Time.deltaTime;
        if (timeToExplode <= 0)
        {
            Explode();
            timeToExplode = explosionCooldown;
        }
    }

    private void Explode() {
        Collider[] colliders = Physics.OverlapSphere(explosionPosition, explosionRadius);

        foreach (Collider itemCollider in colliders) {
            if (itemCollider.gameObject.tag == "Falling") {
                if (itemCollider.TryGetComponent<Rigidbody>(out Rigidbody itemRigidbody))
                {
                    itemRigidbody.AddExplosionForce(explosionForce, explosionPosition, explosionRadius, explosionForce, ForceMode.Impulse);
                }
            }
        }
    }
}
