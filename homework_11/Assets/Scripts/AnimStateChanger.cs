using System.Collections.Generic;
using UnityEngine;

public class AnimStateChanger : MonoBehaviour
{
    private List<string> stateList = new List<string>(){ "swing", "raise", "fall"};
    private Animator anim;

    public void SetRandomState()
    {
        SetAnimState(stateList[Random.Range(0, stateList.Count)]);
    }

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    private void SetAnimState(string newStateName)
    {
        foreach (string state in stateList)
        {
            anim.SetBool(state, state == newStateName);
        }
    }
}
