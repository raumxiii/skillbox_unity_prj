﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskListClass : MonoBehaviour
{
    [SerializeField] private InputField firstField;

    private int maxNumber;

    private void getMaxNumber()
    {
        if (firstField.text != "")
        {
            try
            {
                maxNumber = int.Parse(firstField.text);
            }
            catch
            {
                maxNumber = 0;
                firstField.text = "";
            }
        }
    }

    public void LogEvenNumbers()
    {
        getMaxNumber();

        if (maxNumber > 0) {
            for (int i = 1; i <= maxNumber; i++)
            {
                if (i % 2 == 0)
                {
                    Debug.Log(i.ToString());
                }
            }
        }
    }

    public void LogMultiplyTable()
    {
        getMaxNumber();

        if (maxNumber > 0)
        {
            for (int i = 1; i <= maxNumber; i++)
            {
                string l_string = "";

                for (int j = 1; j <= maxNumber; j++)
                {
                    if (j == 1)
                    {
                        l_string = (i * j).ToString();
                    }
                    else
                    {
                        l_string = l_string + " " + (i * j).ToString();
                    }
                }
                Debug.Log(l_string);
            }
        }
    }

    public void LogFactorial()
    {
        long l_result = 0;

        getMaxNumber();

        if (maxNumber > 20)
        {
            Debug.Log("Не стоит жадничать - введите значение не более 20");
        }
        else if (maxNumber > 0)
        {
            for (int i = 1; i <= maxNumber; i++)
            {
                if (i == 1)
                {
                    l_result = 1;
                }
                else
                {
                    l_result = l_result * i;
                }
            }
        }
        Debug.Log(l_result.ToString());
    }


    public void LogSumMultiplies3n5()
    {
        long l_result = 0;

        getMaxNumber();

        for (int i = 1; i <= maxNumber; i++)
        {
            if (i % 3 == 0 || i % 5 == 0)
            {
                l_result = l_result + i;
            }
        }
        Debug.Log(l_result.ToString());
    }

    public void LogSumFibonacci()
    {
        long l_result = 0;

        getMaxNumber();

        if(maxNumber > 1)
        {
            long l_prev1_value = 0, l_prev2_value = 1, l_curr_value;
            while (true)
            {
                if (l_prev2_value == 1)
                {
                    l_curr_value = 2;
                }
                else
                {
                    l_curr_value = l_prev1_value + l_prev2_value;
                }

                if (l_curr_value > maxNumber)
                {
                    break;
                }

                if (l_curr_value % 2 == 0)
                {
                    l_result = l_result + l_curr_value;
                }
                l_prev1_value = l_prev2_value;
                l_prev2_value = l_curr_value;
            }
        }

        Debug.Log(l_result.ToString());

    }
}
