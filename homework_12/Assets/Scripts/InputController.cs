using UnityEngine;


namespace GlobalValues
{
    [RequireComponent(typeof(PlayerController))]
    public class InputController : MonoBehaviour
    {
        private PlayerController playerController;

        private void Awake()
        {
            playerController = GetComponent<PlayerController>();
        }

        void Update()
        {
            float horizontal_val = Input.GetAxis(GlobalValues.HORIOZONTAL_AXIS);
            float vertical_val = Input.GetAxis(GlobalValues.VERTICAL_AXIS);
            bool isJump = Input.GetButton(GlobalValues.JUMP_BUTTON);
            bool isInteract = Input.GetButton(GlobalValues.INTERACT_BUTTON);

            playerController.Move(horizontal_val, vertical_val);
            playerController.Interact(isInteract);
        }
    }
}