using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagerController : MonoBehaviour
{
    private float timeToRestart;
    public void Update()
    {
        if (timeToRestart == -2f) return;
        else if (timeToRestart > 0) timeToRestart -= Time.deltaTime;
        else RestartLevel();
    }
    
    public void InitLevelRestart(float restartTime = 3f)
    {
        timeToRestart = restartTime;
    }


    private void Awake()
    {
        timeToRestart = -2f;
    }


    private void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

}
