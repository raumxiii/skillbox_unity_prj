using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GlobalValues {

    public class InteractableTrigger : MonoBehaviour
    {
        [SerializeField] private GameObject linkedObject;
        public void Interact()
        {
            if (linkedObject.CompareTag(GlobalValues.REMOVABLE_TAG))
                linkedObject.SetActive(false);
        }
    }
}