using UnityEngine;

public class DeathZoneController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision) {

        HealthComponent healthComponent;
        collision.TryGetComponent<HealthComponent>(out healthComponent);
        if (!healthComponent) healthComponent = collision.GetComponentInParent<HealthComponent>();

        if (healthComponent) healthComponent.GetDamage(healthComponent.GetCurHealth());
    }

}
