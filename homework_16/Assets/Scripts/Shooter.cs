using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    [SerializeField] private GameObject bullet;
    [SerializeField] private Transform bulletPoint;
    [SerializeField] private Transform rearBulletPoint;
    [SerializeField] private float bulletVelocity;
    [SerializeField] private float shootReadyTime = 0f;
    private float curShootReadyTime;
    private bool isShooting;

    private float bulletDirection;

    public bool IsShooting()
    {
        return isShooting;
    }
    public void Shoot(float direction)
    {
        if (IsShooting()) return;

        isShooting = true;
        bulletDirection = Mathf.Sign(direction);
    }

    private void Awake()
    {
        curShootReadyTime = shootReadyTime;
        isShooting = false;
    }

    private void Update()
    {
        if (isShooting)
        {
            if (curShootReadyTime > 0) curShootReadyTime -= Time.deltaTime;
            else
            {
                MakeShoot();
                isShooting = false;
                curShootReadyTime = shootReadyTime;
            }
        }
    }

    private void MakeShoot() {
        Vector3 newBulletPosition = bulletPoint.position;
        if (bulletDirection < 0 && rearBulletPoint) newBulletPosition = rearBulletPoint.position;

        GameObject newBullet = Instantiate(bullet, newBulletPosition, Quaternion.identity);
        Rigidbody2D newBulletRigidBody = newBullet.GetComponent<Rigidbody2D>();
        SpriteRenderer newBulletSpriteRenderer = newBullet.GetComponent<SpriteRenderer>();
        if (bulletDirection < 0) newBulletSpriteRenderer.flipX = true;

        newBulletRigidBody.velocity = new Vector2(bulletVelocity * bulletDirection, newBulletRigidBody.velocity.y);
    }

}
