using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GlobalValues {
    [RequireComponent(typeof(PlayerController))]
    public class InputController : MonoBehaviour
    {
        private PlayerController playerController;
        private void Awake()
        {
            playerController = GetComponent<PlayerController>();
        }
        void Update()
        {
            float horizontalDirection = Input.GetAxis(GobalConstants.HORIZONTAL_AXIS);
            bool isJump = Input.GetButtonDown(GobalConstants.JUMP);
            bool isFire1 = Input.GetButtonDown(GobalConstants.FIRE1);
            bool isFire2 = Input.GetButtonDown(GobalConstants.FIRE2);

            playerController.Move(horizontalDirection);

            if (isJump) {
                playerController.Jump();
            }

            if (isFire1)
            {
                playerController.Attack();
            }

            if (isFire2)
            {
                playerController.Attack();
            }
        }
    }
}

