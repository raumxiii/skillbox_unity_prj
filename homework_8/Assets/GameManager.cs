using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject pointPrefab;
    public GameObject personPrefab;
    public GameObject stickPrefab;

    [Range (2, 100)] public int pointCount;
    [Range(0.1f, 10f)] public float runnerSpeed;
    [Range(0.0f, 3f)] public float passDistance;
    public bool isRelayRace;

    private Vector3[] pointArray;
    private Transform[] personTransformArray;

    private int activePersonIndex = -1;
    private int destinationIndex = 1;
    private bool isForward = true;

    private Transform stickTransform;
    private Transform activeRunnerTransform;

    private void GeneratePointArray() {
        Vector3 newPoint;
        for (int i = 0; i < pointCount; i++)
        {
            while (true)
            {
                newPoint = new Vector3(Random.Range(-30f, 30f), Random.Range(-20f, 20f), Random.Range(-20f, 20f));
                if (i == 0 ) break;

                bool isFree = true;
                for (int j=0; j<i;  j++) {
                    if (Vector3.Distance(newPoint, pointArray[j]) < 4f)
                    {
                        isFree = false;
                        break;
                    }
                }
                if (isFree) break;
            }
            pointArray[i] = newPoint;
        }
    }

    private void GenerateObjects() {
        Quaternion pointQuaternion = new Quaternion();
        GameObject person;

        if (isRelayRace) personTransformArray = new Transform[pointCount];
        else personTransformArray = new Transform[1];        

        for (int i = 0; i < pointCount; i++)
        {
            if (isRelayRace) {
                person = Instantiate(personPrefab, pointArray[i], pointQuaternion);
                personTransformArray[i] = person.GetComponent<Transform>();
                if (i == pointCount-1) personTransformArray[i].LookAt(pointArray[0]);
                else personTransformArray[i].LookAt(pointArray[i+1]);
            }
            else
            {
                if (i == 0) { person = Instantiate(personPrefab, pointArray[i], pointQuaternion);
                    personTransformArray[i] = person.GetComponent<Transform>();
                    personTransformArray[i].LookAt(pointArray[i + 1]);
                }
                Instantiate(pointPrefab, pointArray[i], pointQuaternion);
            }
        }
        GameObject stick = Instantiate(stickPrefab, pointArray[0], pointQuaternion);
        stickTransform = stick.GetComponent<Transform>();
    }

    private void ActivateNextPerson() {
        if (activePersonIndex == personTransformArray.Length-1) activePersonIndex = 0;
        else activePersonIndex++;

        activeRunnerTransform = personTransformArray[activePersonIndex];

        stickTransform.position = activeRunnerTransform.position;
        stickTransform.SetParent(activeRunnerTransform);

    }

    private void MoveActivePerson() {
        activeRunnerTransform.position = Vector3.MoveTowards(activeRunnerTransform.position, pointArray[destinationIndex], Time.deltaTime * runnerSpeed);
    }

    private void ProcessPointReaching()
    {
        if (isRelayRace) {
            pointArray[activePersonIndex] = activeRunnerTransform.position;
            ActivateNextPerson();
            if (destinationIndex == pointCount - 1) destinationIndex = -1;
        }
        else {
            if (destinationIndex == 0) isForward = true;
            else if (destinationIndex == pointCount - 1) isForward = false;
        }

        if (isForward) destinationIndex++;
        else destinationIndex--;

        activeRunnerTransform.LookAt(pointArray[destinationIndex]);
    }

    // Start is called before the first frame update
    void Start()
    {
        pointArray = new Vector3[pointCount];
        GeneratePointArray();
        GenerateObjects();
        ActivateNextPerson();
    }

    // Update is called once per frame
    void Update()
    {
        MoveActivePerson();
        if (Vector3.Distance(activeRunnerTransform.position, pointArray[destinationIndex]) <= passDistance)
        {
            ProcessPointReaching();
        }
    }
}
