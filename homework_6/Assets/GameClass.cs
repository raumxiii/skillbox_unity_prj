﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class GameClass : MonoBehaviour
{
    [SerializeField] private double levelTimeLimit = 10;
    [SerializeField] private int victoryPinValue = 0;

    [SerializeField] private int minPinValue = 0;
    [SerializeField] private int maxPinValue = 10;

    [SerializeField] private bool needRandomTool;

    [SerializeField] private GameObject StartGamePanel;
    [SerializeField] private GameObject VictoryGamePanel;
    [SerializeField] private GameObject DefeatGamePanel;

    [SerializeField] private Text timerText;
    [SerializeField] private Text leftPinText;
    [SerializeField] private Text midPinText;
    [SerializeField] private Text rightPinText;

    [SerializeField] private Text leftToolButtonText;
    [SerializeField] private Text midToolButtonText;
    [SerializeField] private Text rightToolButtonText;

    [SerializeField] private Button leftToolButton;
    [SerializeField] private Button midToolButton;
    [SerializeField] private Button rightToolButton;

    [SerializeField] private int[] leftToolValList  = { 1, -1,  0};
    [SerializeField] private int[] midToolValList   = {-1,  2, -1};
    [SerializeField] private int[] rightToolValList = {0,   1,  2};

    private int[] pinValList = new int[3];

    private System.Random rnd = new System.Random();

    private double timeRemain;
    private bool isInProgress;

    // Обработка события проигрыша в игре. Отключение игрового режима, отображение панели проигрыша.
    private void Defeat()
    {
        DefeatGamePanel.SetActive(true);
        leftToolButton.interactable = false;
        midToolButton.interactable = false;
        rightToolButton.interactable = false;
        isInProgress = false;
    }

    // Обработка события победы в игре. Отключение игрового режима, отображение панели победы.
    private void Victory()
    {
        VictoryGamePanel.SetActive(true);
        leftToolButton.interactable = false;
        midToolButton.interactable = false;
        rightToolButton.interactable = false;
        isInProgress = false;
    }

    // При старте сцены игровой процесс не считается запущенным.
    void Start()
    {
        isInProgress = false;
    }

    // Актуализация таймера, проверка его истечения.
    void Update()
    {
        
        if (!isInProgress)
        {
            return;
        }

        timeRemain -= Time.deltaTime;
        timerText.text = Math.Max(Math.Round(timeRemain), 0).ToString();
        if (timeRemain <= 0)
        {
            Defeat();
        }
    }

    // Актуализировать доступностькнопок инструментов в зависимости от состояния пинов и пределов значений.
    public void ActualisePinButtonState()
    {
        bool isLeftPassed = true, isMidPassed = true, isRightPassed = true;
        for (int i = 0; i < pinValList.Length; i++)
        {
            if ((pinValList[i] + leftToolValList[i] > maxPinValue) || (pinValList[i] + leftToolValList[i] < minPinValue))
            {
                isLeftPassed = false;
            }
            if (pinValList[i] + midToolValList[i] > maxPinValue || pinValList[i] + midToolValList[i] < minPinValue)
            {
                isMidPassed = false;
            }
            if (pinValList[i] + rightToolValList[i] > maxPinValue || pinValList[i] + rightToolValList[i] < minPinValue)
            {
                isRightPassed = false;
            }
        }

        if (isLeftPassed) leftToolButton.interactable = true;
        else leftToolButton.interactable = false;

        if (isMidPassed) midToolButton.interactable = true;
        else midToolButton.interactable = false;

        if (isRightPassed) rightToolButton.interactable = true;
        else rightToolButton.interactable = false;
    }

    // Присвоение стартовых значений Инструментам, пинам, сброс таймера, старт процесса игры.
    public void StartProgress()
    {
        StartGamePanel.SetActive(false);
        leftToolButtonText.text = "";
        midToolButtonText.text = "";
        rightToolButtonText.text = "";

        for (int i = 0; i < pinValList.Length; i++)
        {
            pinValList[i] = 0;
        }

        leftPinText.text  = pinValList[0].ToString();
        midPinText.text   = pinValList[1].ToString();
        rightPinText.text = pinValList[2].ToString();

        for (int i = 0; i < 3; i++)
        {
            if (needRandomTool)
            {
                leftToolValList[i]  = rnd.Next(0, 2);
                midToolValList[i]   = rnd.Next(-2, 2);
                rightToolValList[i] = rnd.Next(1, 2);
            }

            leftToolButtonText.text  = leftToolButtonText.text  + leftToolValList[i].ToString();
            midToolButtonText.text   = midToolButtonText.text   + midToolValList[i].ToString();
            rightToolButtonText.text = rightToolButtonText.text + rightToolValList[i].ToString();
            if (i < 2)
            {
                leftToolButtonText.text = leftToolButtonText.text + "|";
                midToolButtonText.text = midToolButtonText.text + "|";
                rightToolButtonText.text = rightToolButtonText.text + "|";
            }
        }

        leftToolButtonText.text  = leftToolButtonText.text + "\nОтвертка";
        midToolButtonText.text   = midToolButtonText.text + "\nМолоток";
        rightToolButtonText.text = rightToolButtonText.text + "\nОтмычка";

        ActualisePinButtonState();

        timeRemain = levelTimeLimit;
        isInProgress = true;
    }

    // Обработка нажатия кнопок инструментов.
    public void ClickButton(Button presssedButton)
    {
        if (presssedButton != null)
        {
            if (presssedButton == leftToolButton)
            {
                for (int i = 0; i < pinValList.Length; i++)
                {
                    pinValList[i] = pinValList[i] + leftToolValList[i];
                }
            }
            else if (presssedButton == midToolButton)
            {
                for (int i = 0; i < pinValList.Length; i++)
                {
                    pinValList[i] = pinValList[i] + midToolValList[i];
                }
            }
            else if (presssedButton == rightToolButton)
            {
                for (int i = 0; i < pinValList.Length; i++)
                {
                    pinValList[i] = pinValList[i] + rightToolValList[i];
                }
            }
            leftPinText.text  = pinValList[0].ToString();
            midPinText.text   = pinValList[1].ToString();
            rightPinText.text = pinValList[2].ToString();

            ActualisePinButtonState();

            if (pinValList[0] != 0 && pinValList[0] == pinValList[1] && pinValList[1] == pinValList[2])
            {
                if (victoryPinValue == 0 || pinValList[0] == victoryPinValue)
                {
                    Victory();
                }
            }
        }
    }
}
