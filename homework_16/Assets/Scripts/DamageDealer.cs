using UnityEngine;

public class DamageDealer : MonoBehaviour
{

    [SerializeField] private float damage;
    private bool isDamageable = true;
    private Transform myTransform;
    private Rigidbody2D myRigidBody;
    private void Awake()
    {
        myTransform = gameObject.transform;
        myRigidBody = gameObject.GetComponent<Rigidbody2D>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isDamageable)
        {
            if (gameObject.CompareTag("Bullet"))
            {
                isDamageable = false;
            }
            MakeHit(collision.gameObject);
        }
    }
    private void MakeHit(GameObject hitObject) {
        hitObject.TryGetComponent<HealthComponent>(out HealthComponent health);
        if (!health)
            health = hitObject.GetComponentInParent<HealthComponent>();

        if (health)
        {
            health.GetDamage(damage);
        }

        PostHitOperation(hitObject);
    }

    private void PostHitOperation(GameObject hitObject)
    {
        if (gameObject.CompareTag("Bullet"))
        {

            hitObject.TryGetComponent<Rigidbody2D>(out Rigidbody2D hitObjectRigidBody);
            if (!hitObjectRigidBody)
                hitObjectRigidBody = hitObject.GetComponentInParent<Rigidbody2D>();

            if (hitObjectRigidBody)
            {
                myRigidBody.velocity = new Vector2(0, 0);
                myTransform.SetParent(hitObject.transform);

                FixedJoint2D myFixedJoint = gameObject.AddComponent<FixedJoint2D>();
                myFixedJoint.connectedBody = hitObjectRigidBody;
            }
            else Destroy(gameObject);
        }
    }
}
