using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveController : MonoBehaviour
{
    [SerializeField] private PointEffector2D explosiveEffector;
    [SerializeField] private GameObject rootComponent;
    [SerializeField] private float timeout = 1;

    private bool isTriggered = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isTriggered)
        {
            Instantiate<PointEffector2D>(explosiveEffector, GetComponent<Transform>());
            isTriggered = true;
        }
    }

    private void FixedUpdate()
    {
        if (isTriggered) 
        {
            if (timeout > 0) timeout -= Time.deltaTime;
            else {
                Destroy(rootComponent);
            }
        }
        
    }
}
