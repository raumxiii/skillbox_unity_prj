using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(UIController))]
public class PlayerController : MonoBehaviour
{
    [Header("Movement settings")]
    [SerializeField, Range(1f, 10f)] private float movementVelocity = 3f;
    [SerializeField, Range(1f, 5f)] private float jumpVelocity = 3f;
    [SerializeField] private AnimationCurve movementSpeedCurve;

    [Header("Ground contoller values")]
    [SerializeField, Range(0.01f, 0.5f)] private float groundOverlapRadios = 0.02f;
    [SerializeField] private LayerMask groundMask;
    [SerializeField] private Transform groundPointTransform;

    [Header("MiscComponents")]
    [SerializeField] private HealthComponent healthComponent;
    [SerializeField] private UIController uiController;
    [SerializeField] private SceneManagerController sceneManagerController;


    private Rigidbody2D playerRigidbody;
    private SpriteRenderer playerSpriteRenderer;
    private AnimatorController playerAnimatorController;
    private bool isGrounded;
    private float playerDirection;
    private bool isDead; 

    private void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        playerSpriteRenderer = GetComponent<SpriteRenderer>();
        TryGetComponent<AnimatorController>(out playerAnimatorController);
        isGrounded = true;
        playerDirection = 1f;
        isDead = false;
    }

    private void FixedUpdate()
    {
        uiController.ShowPlayerHealth(healthComponent.GetCurHealth());
        if (isDead) return;

        Vector2 overlapCapsulePosition = groundPointTransform.position;
        Physics2D.alwaysShowColliders = true;
        isGrounded = Physics2D.OverlapCircle(overlapCapsulePosition, groundOverlapRadios, groundMask);

        if (playerAnimatorController) {
            if (isGrounded) playerAnimatorController.StopJumping();
            else playerAnimatorController.StartJumping();
        }

        if (!healthComponent.IsAlive())
        {
            uiController.ShowDeathScreen();
            sceneManagerController.InitLevelRestart();
            isDead = true;
        }
    }

    public void Move(float direction)
    {
        if (isDead) return;
        if (Mathf.Abs(direction) > 0.01f)
        {
            playerRigidbody.velocity = new Vector2(movementSpeedCurve.Evaluate(direction) * movementVelocity, playerRigidbody.velocity.y);
            float newDirection = Mathf.Sign(direction);
            if (playerDirection != newDirection)
            {
                playerSpriteRenderer.flipX = !playerSpriteRenderer.flipX;
                playerDirection = Mathf.Sign(direction);
            }

            if (isGrounded && playerAnimatorController) playerAnimatorController.StartRunning();
        }
        else
        {
            if(playerAnimatorController) playerAnimatorController.StopRunning();
        }
    }
    public void Jump()
    {
        if (isDead) return;
        if (isGrounded)
        {
            playerRigidbody.velocity = new Vector2(playerRigidbody.velocity.x, jumpVelocity);
        }
    }

    public void Attack()
    {
        if (isDead) return;
        if (isGrounded && gameObject.TryGetComponent<Shooter>(out Shooter shooter))
        {
            if (!shooter.IsShooting())
            {
                if (playerAnimatorController) playerAnimatorController.StartShooting();
                shooter.Shoot(playerDirection);
            }
        }
    }
}
