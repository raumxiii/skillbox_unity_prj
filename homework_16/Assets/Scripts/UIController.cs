using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] private Text playerHealthText;
    [SerializeField] private GameObject gameOverPanel;
    public void Awake()
    {
        
    }

    public void ShowPlayerHealth(float playerHealth)
    {
        if (playerHealthText)
            playerHealthText.text = playerHealth.ToString();
    }

    public void ShowDeathScreen()
    {
        if (gameOverPanel)
            gameOverPanel.SetActive(true);
    }
}
