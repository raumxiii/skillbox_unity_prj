﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class CalculatorClass : MonoBehaviour
{
    [SerializeField] private InputField firstField;
    [SerializeField] private InputField secondField;
    [SerializeField] private Text result;

    private float a = 0, b = 0, g_result;

    private string getValues()
    {
        if (firstField.text != "")
        {
            try
            {
                a = float.Parse(firstField.text);
            }
            catch
            {
                return "Первое поле должно быть числом!";
            }
        }

        if (secondField.text != "")
        {
            try
            {
                b = float.Parse(secondField.text);
            }
            catch
            {
                return "Второе поле должно быть числом!";
            }
        }
        return "";
    }

    public void Plus()
    {
        string l_result;

        l_result = getValues();
        if (l_result == "")
        {
            g_result = a + b;
            l_result = g_result.ToString();
        }
        result.text = l_result;
    }

    public void Minus()
    {
        string l_result;

        l_result = getValues();
        if (l_result == "")
        {
            g_result = a - b;
            l_result = g_result.ToString();
        }
        result.text = l_result;
    }

    public void Multiply()
    {
        string l_result;

        l_result = getValues();
        if (l_result == "")
        {
            g_result = a * b;
            l_result = g_result.ToString();
        }
        result.text = l_result;
    }

    public void Div()
    {
        string l_result;

        l_result = getValues();
        if (l_result == "")
        {
            if (b == 0)
            {
                g_result = 0;
                if (a == 0)
                {
                    l_result = "Спорный вопрос";
                }
                else
                {
                    l_result = "Бесконечность";
                }
            }
            else
            {
                g_result = a / b;
                l_result = g_result.ToString();
            }
        }

        result.text = l_result;
    }
}
