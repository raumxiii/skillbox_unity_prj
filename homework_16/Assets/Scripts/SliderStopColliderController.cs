using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliderStopColliderController : MonoBehaviour
{
    [SerializeField] private SliderJoint2D linkedSlider;
    [SerializeField] private Collider2D linkedCollider;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision == linkedCollider) {
            JointMotor2D linkedMotor = linkedSlider.motor;
            linkedMotor.motorSpeed = -1* linkedMotor.motorSpeed;
            linkedSlider.motor = linkedMotor;
        }
    }
}

