using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform orientSpotTransform;

    private Vector3 offset;
    private void Start()
    {
        offset = transform.position - orientSpotTransform.position;
    }


    private void FixedUpdate()
    {
        transform.position = orientSpotTransform.position + offset;
    }
}
