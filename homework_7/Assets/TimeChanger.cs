using System;
using UnityEngine;
using UnityEngine.UI;

public class TimeChanger : MonoBehaviour
{
    public Image foreGround;
    public Image arrow;
    public bool Tick = false;
    public Text remainText;

    private float maxTime = -2;
    private float curTime;
    private AudioSource onTickAudio;

    public void SetMaxTime(float timeValue) {
        maxTime = timeValue;
        if (timeValue != -2)
            curTime = timeValue;            
    }

    public void BlockTimer() {
        maxTime = -2;
        arrow.transform.rotation.Set(0f, 0f, 0f, 0f);
    }

    // Start is called before the first frame update
    void Start()
    {
        onTickAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        Tick = false;
        if (maxTime == -2) return;
        
        curTime -= Time.deltaTime;
        if (curTime <= 0)
        {
            Tick = true;
            curTime = maxTime;
            onTickAudio.Play();
        }

        if (remainText) {
            remainText.text = Math.Round(curTime).ToString();
        }

        foreGround.fillAmount = curTime / maxTime;

        arrow.transform.Rotate(0f, 0f, 360-Time.deltaTime / maxTime * 360f);
    }
}
