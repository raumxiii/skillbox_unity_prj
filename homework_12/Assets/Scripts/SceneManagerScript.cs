using UnityEngine;
using UnityEngine.SceneManagement;

namespace GlobalValues
{
    public class SceneManagerScript : MonoBehaviour
    {

        private float timeTillStartLevel = -2f;
        private int nextSceneIndex;

        public void Awake()
        {
            timeTillStartLevel = -2f;
            nextSceneIndex = SceneManager.GetActiveScene().buildIndex;
        }
        public void LoadScene(int sceneIndex)
        {
            SceneManager.LoadScene(sceneIndex);
            timeTillStartLevel = -2f;
            nextSceneIndex = sceneIndex;
        }

        private int GetNextSceneIdx() {
            return SceneManager.GetActiveScene().buildIndex + 1;
        }

        public void GotoNextScene(float loadDelay =0.5f) {
            int nextSceneIndex_ = GetNextSceneIdx();
            if (nextSceneIndex_ != nextSceneIndex) {
                nextSceneIndex = nextSceneIndex_;
                timeTillStartLevel = loadDelay;
            }
        }

        public void ReloadScene(float reloadDelay)
        {
            timeTillStartLevel = reloadDelay;
        }

        private void FixedUpdate()
        {

            if (timeTillStartLevel == -2) return;
            else
            {
                if (timeTillStartLevel > 0) timeTillStartLevel -= Time.deltaTime;
                else LoadScene(nextSceneIndex);
            }
        }
    }
}