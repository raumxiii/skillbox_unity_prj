using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace GlobalValues
{
    [RequireComponent(typeof(Rigidbody))]
    public class PlayerController : MonoBehaviour
    {
        [SerializeField, Range(1f, 10f)] private float speed = 10f;
        [SerializeField, Range(1f, 5f)] private float dieTimeout = 5f;
        [SerializeField] private GameObject uiHintPanelObject;
        [SerializeField] GameObject playerVisualComponent;
        [SerializeField] private SceneManagerScript sceneManager;

        private Rigidbody playerRigidboy;
        private Vector3 movement;
        private bool isInteract;
        private bool isDead;

        private void Awake()
        {
            playerRigidboy = GetComponent<Rigidbody>();
            uiHintPanelObject.GetComponentInChildren<Text>().text = "Press " + GlobalValues.INTERACT_BUTTON + " Button";
            isDead = false;
        }

        public void Move(float horizontal_val, float vertical_val)
        {
            movement = new Vector3(horizontal_val, 0, vertical_val) * speed;
        }

        public void Interact(bool isEnable)
        {
            isInteract = isEnable;
        }

        private void FixedUpdate()
        {

            if (!isDead)
            {
                MoveCharacter();
            }
        }

        private void MoveCharacter()
        {
            playerRigidboy.AddForce(movement);
        }


        private void Die()
        {
            
            if (TryGetComponent<ParticleSystem>(out ParticleSystem particleSystem)) particleSystem.Play();
            if (playerVisualComponent) Destroy(playerVisualComponent);
            playerRigidboy.isKinematic = true;
            isDead = true;
            sceneManager.ReloadScene(dieTimeout);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(GlobalValues.DEATH_TAG)) Die();
            if (other.CompareTag(GlobalValues.LEVEL_EXIT_TAG))
            {
              sceneManager.GotoNextScene();
            }
            if (other.CompareTag(GlobalValues.INTERACTABLE_TAG)) {
                uiHintPanelObject.SetActive(true);
            }

        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag(GlobalValues.INTERACTABLE_TAG))
            {
                uiHintPanelObject.SetActive(false);
            }
        }

        private void OnTriggerStay(Collider other)
        { 
            if (other.CompareTag(GlobalValues.INTERACTABLE_TAG) && isInteract)
            {
                foreach (InteractableTrigger obj in other.GetComponents<InteractableTrigger>())
                    obj.Interact();
                
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.CompareTag(GlobalValues.DEATH_TAG)) Die();
        }

#if UNITY_EDITOR
        [ContextMenu("Reset Values")]
        public void ResetValues()
        {
            speed = 10f;
        }

#endif
    }
}