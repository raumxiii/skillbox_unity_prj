using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public Button peasantHireButton;
    public Button warriorHireButton;

    public Text resourceCountText;
    public Text nextWaveEnemyCountText;

    public TimeChanger harvestTimer;
    public TimeChanger eatimgTimer;
    public TimeChanger nextWaveTimer;
    public TimeChanger hirePeasantTimer;
    public TimeChanger hireWarriorTimer;

    public GameObject hireButtonPanel;

    public GameObject gameMainScreenFader;
    public GameObject gameMainScreen;
    public GameObject gameOverScreen;
    public Text gameOverStatText;

    public Button slowTimeButton;
    public Button stopTimeButton;
    public Button playTimeButton;
    public Button muteButton;

    public Sprite muteOnSprite;
    public Sprite muteOffSprite;
    public Sprite defeatScreenSprite;
    public Sprite victoryScreenSprite;

    public int initPeasantCount;
    public int initWarriorCount;
    public int initGrainCount;

    public float harvestGrownTime;
    public float eatingCycleTime;

    public float peasantHireTime;
    public int peasantCostInGrain;
    public int peasantGrainConsumption;
    public int grainGrownPerPeasant;

    public float warriorHireTime;
    public int warriorCostInGrain;
    public int warriorGrainConsumption;

    public float enemyWaveTime;
    public int firstWaveEnemyCount;
    public int firstEnemyWaveTurn;
    public int enemyWaveIncrement;

    public int vicroryTurnNumber;
    public int vicroryGrainCount;
    public int vicroryPeasantCount;

    private int peasantCount;
    private int warriorCount;
    private int grainCount;

    private int curTurn;
    private int maxTurnAlived;
    private int maxGrainCount;
    private int totalGrainHarvested;
    private int totalFallenWarriorCount;

    private int nextWaveEnemyCount;
    private bool isWarriorHiring;
    private bool isPeasantHiring;

    private bool isPaused = false;
    private bool isMute = false;
    private Image gameOverImage;
    private Image muteButtonImage;
    private Image gameMainScreenFaderImage;
    private Color playGameFaderColor = new Color(0, 0, 0, 0.2f);
    private Color stopGameFaderColor = new Color(0, 0, 0, 0.8f);

    // Start is called before the first frame update
    void Start()
    {
        gameOverImage = gameOverScreen.GetComponent<Image>();
        muteButtonImage = muteButton.GetComponent<Image>();
        gameMainScreenFaderImage = gameMainScreenFader.GetComponent<Image>();

        curTurn = 1;
        maxTurnAlived = 0;
        totalFallenWarriorCount = 0;
        isWarriorHiring = false;
        isPeasantHiring = false;

        peasantCount = initPeasantCount;
        warriorCount = initWarriorCount;
        grainCount = initGrainCount;

        if (firstEnemyWaveTurn <= curTurn)
            nextWaveEnemyCount = firstWaveEnemyCount;
        else
            nextWaveEnemyCount = 0;

        maxGrainCount = initGrainCount;
        totalGrainHarvested = initGrainCount;

        harvestTimer.SetMaxTime(harvestGrownTime);
        nextWaveTimer.SetMaxTime(enemyWaveTime);
        eatimgTimer.SetMaxTime(eatingCycleTime);

        PlayTime();
        Update();
    }

   // Update is called once per frame
    void Update()
    {
        if (isPaused)
            return;

        CheckTimers();
        UpdateButtons();
        UpdateCountText();
        CheckEndGameConditions();
        EndUpdate();
    }


    public void Restart()
    {
        SceneManager.LoadScene(0);
    }

    public void SlowTime()
    {
        isPaused = false;
        Time.timeScale = 0.5f;
        slowTimeButton.interactable = false;
        stopTimeButton.interactable = true;
        playTimeButton.interactable = true;
        hireButtonPanel.SetActive(true);
        gameMainScreenFaderImage.color = playGameFaderColor;
    }

    public void StopTime()
    {
        isPaused = true;
        Time.timeScale = 0f;
        slowTimeButton.interactable = true;
        stopTimeButton.interactable = false;
        playTimeButton.interactable = true;
        hireButtonPanel.SetActive(false);
        gameMainScreenFaderImage.color = stopGameFaderColor;
    }

    public void PlayTime()
    {
        isPaused = false;
        Time.timeScale = 1f;
        slowTimeButton.interactable = true;
        stopTimeButton.interactable = true;
        playTimeButton.interactable = false;
        hireButtonPanel.SetActive(true);
        gameMainScreenFaderImage.color = playGameFaderColor;
    }

    public void MuteSound()
    {
        isMute = !isMute;
        if (isMute)
        {
            AudioListener.volume = 0;
            muteButtonImage.sprite = muteOnSprite;
        }
        else
        {
            AudioListener.volume = 1;
            muteButtonImage.sprite = muteOffSprite;
        }
    }

    public void HirePeasant()
    {
        grainCount -= peasantCostInGrain;
        hirePeasantTimer.SetMaxTime(peasantHireTime);
        isPeasantHiring = true;
    }

    public void HireWarrior()
    {
        grainCount -= warriorCostInGrain;
        hireWarriorTimer.SetMaxTime(warriorHireTime);
        isWarriorHiring = true;
    }

    private void UpdateButtons()
    {
        if (!isWarriorHiring && grainCount >= warriorCostInGrain)
            warriorHireButton.interactable = true;
        else
            warriorHireButton.interactable = false;

        if (!isPeasantHiring && grainCount >= peasantCostInGrain)
            peasantHireButton.interactable = true;
        else
            peasantHireButton.interactable = false;
    }

    private void CheckTimers() {
        if (harvestTimer.Tick)
        {
            int grainIncrement = peasantCount * grainGrownPerPeasant;
            grainCount += grainIncrement;
            totalGrainHarvested += grainIncrement;
            if (maxGrainCount < grainCount)
                maxGrainCount = grainCount;
        }
        if (eatimgTimer.Tick)
        {
            grainCount -= (peasantCount * peasantGrainConsumption + warriorCount * warriorGrainConsumption);
            if (grainCount < 0) grainCount = 0;
        }
        if (nextWaveTimer.Tick)
        {
            totalFallenWarriorCount += Math.Min(warriorCount, nextWaveEnemyCount);
            warriorCount -= nextWaveEnemyCount;
        }
        if (hirePeasantTimer.Tick)
        {
            peasantCount++;
            hirePeasantTimer.BlockTimer();
            isPeasantHiring = false;
        }
        if (hireWarriorTimer.Tick)
        {
            warriorCount++;
            hireWarriorTimer.BlockTimer();
            isWarriorHiring = false;
        }
    }

    private bool IsDefeatEndGameConditions()
    {
        return (warriorCount < 0);
    }

    private bool IsVictoryEndGameConditions()
    {
        if (vicroryGrainCount > 0 && grainCount >= vicroryGrainCount ||
           vicroryPeasantCount > 0 && peasantCount >= vicroryPeasantCount ||
           vicroryTurnNumber > 0 && maxTurnAlived >= vicroryTurnNumber)
            return true;

        return false;
    }

    private void CheckEndGameConditions() {
        if (IsVictoryEndGameConditions()) GameOver(true);
        if (IsDefeatEndGameConditions()) GameOver(false);
    }

    private void GameOver(bool isVictory) {
        StopTime();
        if (isVictory) gameOverImage.sprite = victoryScreenSprite;
        else gameOverImage.sprite = defeatScreenSprite;
        if (gameOverScreen)
            gameOverScreen.SetActive(true);
        if (gameMainScreen)
            gameMainScreen.SetActive(false);
        if (gameOverStatText)
            gameOverStatText.text = maxTurnAlived + "\n" + maxGrainCount + "\n" + totalGrainHarvested + "\n" + peasantCount + "\n" + totalFallenWarriorCount;
    }

    private void EndUpdate() {
        if (nextWaveTimer.Tick)
        {
            EndTurn();
        }
    }

    private void EndTurn() {
        curTurn++;
        maxTurnAlived++;
        if (curTurn > firstEnemyWaveTurn)
            nextWaveEnemyCount += enemyWaveIncrement;
        else if (curTurn == firstEnemyWaveTurn)
            nextWaveEnemyCount = firstWaveEnemyCount;
    }

    private void UpdateCountText() {
        resourceCountText.text = peasantCount + "\n" + warriorCount + "\n\n" + grainCount;
        nextWaveEnemyCountText.text = nextWaveEnemyCount.ToString();
    }
}
