﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class ThreeNumberComparerClass : MonoBehaviour
{
    [SerializeField] private InputField firstField;
    [SerializeField] private InputField secondField;
    [SerializeField] private InputField thirdField;

    [SerializeField] private Text result;

    public void Compare()
    {
        float a = 0, b = 0, c = 0, l_min_val;
        string l_result;

        if (firstField.text != "")
        {
            try
            {
                a = float.Parse(firstField.text);
            }
            catch
            {
                firstField.text = a.ToString();
            }
        }

        if (secondField.text != "")
        {
            try
            {
                b = float.Parse(secondField.text);
            }
            catch
            {
                secondField.text = b.ToString();
            }
        }

        if (thirdField.text != "")
        {
            try
            {
                c = float.Parse(thirdField.text);
            }
            catch
            {
                thirdField.text = c.ToString();
            }
        }

        if (a == b && b == c)
        {
            l_result = "Все числа равны.";
        }
        else
        {
            l_min_val = a;
            if (l_min_val > b)
            {
                l_result = l_min_val.ToString();
                l_min_val = b;
            }
            else
            {
                l_result = b.ToString();
            }

            l_result = l_result + "; " + Math.Max(l_min_val, c).ToString();
        }

        result.text = l_result;
    }
}