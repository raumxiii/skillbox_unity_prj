using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GlobalValues {

    public class BonusController : MonoBehaviour
    {
        [SerializeField] float timeToDestroy = 4f;
        private bool isTriggered = false;
        private void OnTriggerEnter(Collider other)
        {
            if (!isTriggered)
            {
                if (other.CompareTag(GlobalValues.PLAYER_TAG))
                {
                    if (TryGetComponent<ParticleSystem>(out ParticleSystem partSystem)) partSystem.Play();
                    if (TryGetComponent<Animator>(out Animator animator)) animator.SetBool("Die", true);

                    isTriggered = true;
                    Destroy(gameObject, timeToDestroy);
                }
            }
    }
    }
}