using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitySpereController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent<Rigidbody>(out Rigidbody otherRigidbody))
        {
            otherRigidbody.useGravity = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.TryGetComponent<Rigidbody>(out Rigidbody otherRigidbody))
        {
            otherRigidbody.useGravity = true;
        }
    }
}
