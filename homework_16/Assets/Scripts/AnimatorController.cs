using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorController : MonoBehaviour
{
    [SerializeField] private Animator animator;
    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void StartShooting()
    {
        animator.SetBool("IsShooting", true);
    }

    public void StopShooting()
    {
        animator.SetBool("IsShooting", false);
    }

    public void StartRunning()
    {
        animator.SetBool("IsRunning", true);
    }

    public void StopRunning()
    {
        animator.SetBool("IsRunning", false);
    }

    public void StartAttacking()
    {
        animator.SetBool("IsAttacking", true);
    }

    public void StopAttacking()
    {
        animator.SetBool("IsAttacking", false);
    }

    public void StartJumping()
    {
        animator.SetBool("IsJumping", true);
    }

    public void StopJumping()
    {
        animator.SetBool("IsJumping", false);
    }


    public void Die()
    {
        animator.SetBool("IsDead", true);
    }
    
}
