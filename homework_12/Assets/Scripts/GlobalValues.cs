namespace GlobalValues
{
    public class GlobalValues
    {
        #region String Input Control Values
        public const string HORIOZONTAL_AXIS = "Horizontal";
        public const string VERTICAL_AXIS = "Vertical";
        public const string JUMP_BUTTON = "Jump";
        public const string INTERACT_BUTTON = "Interact";
        #endregion

        #region Tag Values
        public const string PLAYER_TAG = "Player";
        public const string DEATH_TAG = "Death";
        public const string LEVEL_EXIT_TAG = "LevelExit";
        public const string INTERACTABLE_TAG = "Interactable";
        public const string REMOVABLE_TAG = "Removable";
        #endregion
    }
}
