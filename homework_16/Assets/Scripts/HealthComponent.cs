using UnityEngine;

public class HealthComponent : MonoBehaviour
{
    [SerializeField] private GameObject rootGameObject;
    [SerializeField, Range(1f, 10f)] private float maxHealth;
    [SerializeField] private float curHealth;

    [SerializeField] private float deathFadeTime = 10f;

    private float fadeTime;
    private AnimatorController ainmatorController;

    public float GetCurHealth()
    {
        return curHealth;
    }

    public void GetDamage(float damage)
    {
        curHealth -= damage;
        if (!IsAlive())
        {
            Die();
        }
    }

    public bool IsAlive()
    {
        return (curHealth > 0);
    }

    private void Awake()
    {
        curHealth = maxHealth;
        fadeTime = -2f;

        gameObject.TryGetComponent<AnimatorController>(out ainmatorController);
        if (!ainmatorController)
            ainmatorController = GetComponentInParent<AnimatorController>();
        
    }

    private void Die()
    {
        fadeTime = deathFadeTime;
        if (ainmatorController) ainmatorController.Die();
    }

    private void FixedUpdate()
    {
        if (fadeTime == -2) return;
        else if (fadeTime > 0) fadeTime -= Time.deltaTime;
        else Destroy(rootGameObject);
    }
}
