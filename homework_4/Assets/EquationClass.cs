﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;


public class EquationClass : MonoBehaviour
{
    [SerializeField] private InputField firstField;
    [SerializeField] private InputField secondField;
    [SerializeField] private InputField thirdField;

    [SerializeField] private Text result;

    public void Calculate()
    {
        double a = 0, b = 0, c = 0, x1, x2, d;
        string l_result = "";

        if (firstField.text != "")
        {
            try
            {
                a = double.Parse(firstField.text);
            }
            catch
            {
                firstField.text = a.ToString();
            }
        }
        else
        {
            firstField.text = a.ToString();
        }

        if (a == 0)
        {
            result.text = "Первое число не должно равняться 0!";
            return;
        }

        if (secondField.text != "")
        {
            try
            {
                b = double.Parse(secondField.text);
            }
            catch
            {
                secondField.text = b.ToString();
            }
        }

        if (thirdField.text != "")
        {
            try
            {
                c = float.Parse(thirdField.text);
            }
            catch
            {
                thirdField.text = c.ToString();
            }
        }

        d = (b * b) - (4 * a * c);
        if (d > 0)
        {
            x1 = (-b + Math.Sqrt(d)) / (2 * a);
            x2 = (-b - Math.Sqrt(d)) / (2 * a);

            l_result = x1.ToString() + "; " + x2.ToString();
        }
        else if (d == 0)
        {
            x1 = -b / (2 * a);
            l_result = x1.ToString();
        }
        else
        {
            l_result = "решения не имеет";
        }
            
        result.text = l_result;
    }
}
