using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BilliardMainBallController : MonoBehaviour
{
    private float force = 50;
    // Start is called before the first frame update
    void Start()
    {
        Rigidbody rigidbody = transform.GetComponent<Rigidbody>();
        rigidbody.AddForce(new Vector3(0, 0, force), ForceMode.Impulse);
    }
}
