﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachina : MonoBehaviour
{
    [SerializeField] private GameObject CalculatorPanel;
    [SerializeField] private GameObject ComparerPanel;
    [SerializeField] private GameObject TripleComparerPanel;
    [SerializeField] private GameObject EquationPanel;

    private GameObject currentPanel;

    public void SetPanel(GameObject newPanel)
    {
        if (currentPanel != null)
        {
            currentPanel.SetActive(false);
        }

        currentPanel = newPanel;
        currentPanel.SetActive(true);
    }
}
