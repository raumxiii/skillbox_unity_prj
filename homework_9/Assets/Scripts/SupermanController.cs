using UnityEngine;

public class SupermanController : MonoBehaviour
{

    [Range (100f, 100000f)] public float punchForce;

    private Rigidbody selfRigidbody;

    // Start is called before the first frame update
    void Start()
    {
        selfRigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        selfRigidbody.AddForce(punchForce, 0, 0, ForceMode.Force);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent<Rigidbody>(out Rigidbody collisionRigidbody))
        {
            collisionRigidbody.AddForceAtPosition(selfRigidbody.centerOfMass, collisionRigidbody.centerOfMass, ForceMode.Impulse);
            collisionRigidbody.useGravity = true;
        }
    }
}