﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class TwoNumberComparerClass : MonoBehaviour
{
    [SerializeField] private InputField firstField;
    [SerializeField] private InputField secondField;

    [SerializeField] private Text result;

    public void Compare()
    {
        float a = 0, b = 0;
        string l_result;

        if (firstField.text != "")
        {
            try
            {
                a = float.Parse(firstField.text);
            }
            catch
            {
                firstField.text = a.ToString();
            }
        }

        if (secondField.text != "")
        {
            try
            {
                b = float.Parse(secondField.text);
            }
            catch
            {
                secondField.text = b.ToString();
            }
        }

        if (a == b)
        {
            l_result = "Числа равны.";
        }
        else
        {
            l_result = Math.Max(a, b).ToString();
        }

        result.text = l_result;
    }
}
