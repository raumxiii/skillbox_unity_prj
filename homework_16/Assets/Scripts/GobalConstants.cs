namespace GlobalValues
{
    public class GobalConstants
    {
        #region Movement

        public const string HORIZONTAL_AXIS = "Horizontal";
        public const string VERTICAL_AXIS = "Vertical";
        public const string JUMP = "Jump";
        public const string FIRE1 = "Fire1";
        public const string FIRE2 = "Fire2";

        #endregion
    }
}